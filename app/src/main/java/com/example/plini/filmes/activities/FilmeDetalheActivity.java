package com.example.plini.filmes.activities;

import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.plini.filmes.R;

public class FilmeDetalheActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filme_detalhe);

        getSupportActionBar().hide();

        //Pegando os dados

        String titulo = getIntent().getExtras().getString("filme_nome");
        String subtitulo = getIntent().getExtras().getString("filme_subtitulo");
        String duracao = getIntent().getExtras().getString("filme_duracao");
        String sinopse = getIntent().getExtras().getString("filme_sinopse");
        String img_url = getIntent().getExtras().getString("filme_img");

        //inicializando as views

        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.collapsingtoolbar_id);
        collapsingToolbarLayout.setTitleEnabled(true);

        TextView tv_titulo = findViewById(R.id.aa_nome_filme);
        TextView tv_subtitulo = findViewById(R.id.aa_subtitulo_filme);
        TextView tv_duracao = findViewById(R.id.aa_duracao_filme);
        TextView tv_sinopse = findViewById(R.id.aa_sinopse);
        ImageView img = findViewById(R.id.aa_thumbnail);

        //Dando valor as views

        tv_titulo.setText(titulo);
        tv_subtitulo.setText(subtitulo);
        tv_duracao.setText(duracao);
        tv_sinopse.setText(sinopse);

        collapsingToolbarLayout.setTitle(titulo);

        //Usando Glide para a imagem

        Glide.with(this).load(img_url).into(img);

    }
}
