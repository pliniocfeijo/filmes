package com.example.plini.filmes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.plini.filmes.R;
import com.google.firebase.auth.FirebaseAuth;

public class ListaFilmes extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_filmes);

        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.logoutMenu:
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(ListaFilmes.this, MainActivity.class));
                break;
            case R.id.filmesLista:
                break;
            case R.id.favoritosLista:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
