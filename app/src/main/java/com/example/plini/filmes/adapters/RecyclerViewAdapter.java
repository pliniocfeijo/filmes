package com.example.plini.filmes.adapters;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.plini.filmes.activities.FilmeDetalheActivity;
import com.example.plini.filmes.model.Filme;
import com.example.plini.filmes.R;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {

    private Context mContext;
    private List<Filme> mData;
    RequestOptions option;

    public RecyclerViewAdapter(Context mContext, List<Filme> mData) {
        this.mContext = mContext;
        this.mData = mData;

        option = new RequestOptions().centerCrop().placeholder(R.drawable.loading_shape).error(R.drawable.loading_shape);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        LayoutInflater inflater = LayoutInflater.from(mContext);
        view = inflater.inflate(R.layout.activity_lista_filmes,parent, false);
        final MyViewHolder viewHolder = new MyViewHolder(view);
        viewHolder.view_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, FilmeDetalheActivity.class);
                i.putExtra("filme_nome", mData.get(viewHolder.getAdapterPosition()).getTitulo_filme());
                i.putExtra("filme_subtitulo", mData.get(viewHolder.getAdapterPosition()).getSubTitulo_filme());
                i.putExtra("filme_duracao", mData.get(viewHolder.getAdapterPosition()).getDuracao_filme());
                i.putExtra("filme_sinopse", mData.get(viewHolder.getAdapterPosition()).getSinopse_filme());
                i.putExtra("filme_img", mData.get(viewHolder.getAdapterPosition()).getUrl_imagem());

                mContext.startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.tv_titulo.setText(mData.get(position).getTitulo_filme());
        holder.tv_subtitulo.setText(mData.get(position).getSubTitulo_filme());
        holder.tv_duracao.setText(mData.get(position).getDuracao_filme());

        Glide.with(mContext).load(mData.get(position).getUrl_imagem()).apply(option).into(holder.img_thumbnail);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView tv_titulo;
        TextView tv_duracao;
        TextView tv_subtitulo;
        ImageView img_thumbnail;
        LinearLayout view_container;

        public MyViewHolder(View itemView){

            super(itemView);

            view_container = itemView.findViewById(R.id.container);
            tv_titulo = itemView.findViewById(R.id.nome_filme);
            tv_duracao = itemView.findViewById(R.id.duracao_filme);
            tv_subtitulo = itemView.findViewById(R.id.subtitulo_filme);
            img_thumbnail = itemView.findViewById(R.id.thumbnail);
        }
    }
}
