package com.example.plini.filmes.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.plini.filmes.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class CriarConta extends AppCompatActivity {

    private EditText nomeUsuario, senhaUsuario, emailUsuario;
    private Button botaoRegistrar;
    private TextView usuarioCadastrado;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_conta);
        setUIViews();

        firebaseAuth = FirebaseAuth.getInstance();

        //"Tarefa" feita pelo botao registrar
        botaoRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(preenchido()){
                    String email_usuario = emailUsuario.getText().toString().trim();
                    String senha_usuario = senhaUsuario.getText().toString().trim();

                    firebaseAuth.createUserWithEmailAndPassword(email_usuario, senha_usuario).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if(task.isSuccessful()){
                                Toast.makeText(CriarConta.this, "Conta criada com sucesso", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(CriarConta.this, MainActivity.class));
                            }else{
                                Toast.makeText(CriarConta.this, "Falha ao criar conta", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

        //"Tarefa" feita pelo textView de usuario cadastrado
        usuarioCadastrado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CriarConta.this, MainActivity.class));
            }
        });
    }

    private void setUIViews(){
        nomeUsuario = findViewById(R.id.editTextNomeUsuario);
        senhaUsuario = findViewById(R.id.editTextSenhaUsuario);
        emailUsuario = findViewById(R.id.editTextEmailUsuario);
        botaoRegistrar = findViewById(R.id.buttonCriarConta);
        usuarioCadastrado = findViewById(R.id.textViewJaTemConta);
    }

    //faz a checagem se todos os campos estao preenchidos
    private Boolean preenchido(){
        Boolean resultado = false;

        String nome = nomeUsuario.getText().toString();
        String senha = senhaUsuario.getText().toString();
        String email = emailUsuario.getText().toString();

        if(nome.isEmpty() || senha.isEmpty() || email.isEmpty()){
            Toast.makeText(CriarConta.this, "Preencha todos os campos, por favor", Toast.LENGTH_LONG).show();
        }else{
            resultado = true;
        }
        return resultado;
    }
}
