package com.example.plini.filmes.model;

public class Filme {

    private String titulo_filme;
    private String subTitulo_filme;
    private String duracao_filme;
    private String sinopse_filme;
    private String url_imagem;

    public Filme() {
    }

    public Filme(String titulo_filme, String subTitulo_filme, String duracao_filme, String sinopse_filme, String url_imagem) {
        this.titulo_filme = titulo_filme;
        this.subTitulo_filme = subTitulo_filme;
        this.duracao_filme = duracao_filme;
        this.sinopse_filme = sinopse_filme;
        this.url_imagem = url_imagem;
    }

    public String getTitulo_filme() {
        return titulo_filme;
    }

    public String getSubTitulo_filme() {
        return subTitulo_filme;
    }

    public String getDuracao_filme() {
        return duracao_filme;
    }

    public String getSinopse_filme() {
        return sinopse_filme;
    }

    public String getUrl_imagem() {
        return url_imagem;
    }

    public void setTitulo_filme(String titulo_filme) {
        this.titulo_filme = titulo_filme;
    }

    public void setSubTitulo_filme(String subTitulo_filme) {
        this.subTitulo_filme = subTitulo_filme;
    }

    public void setDuracao_filme(String duracao_filme) {
        this.duracao_filme = duracao_filme;
    }

    public void setSinopse_filme(String sinopse_filme) {
        this.sinopse_filme = sinopse_filme;
    }

    public void setUrl_imagem(String url_imagem) {
        this.url_imagem = url_imagem;
    }
}
