package com.example.plini.filmes.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.plini.filmes.R;
import com.example.plini.filmes.adapters.RecyclerViewAdapter;
import com.example.plini.filmes.model.Filme;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TelaFilmes extends AppCompatActivity {

    private final String JSON_URL = "https://gist.githubusercontent.com/pliniocf/9fffdd40f357b8d937e0a125d1ba7384/raw/5c65b0da24adbc0f522b77a69f308cc0b17a24b9/filmes.json";
    private JsonArrayRequest request;
    private RequestQueue requestQueue;
    private List<Filme> lstFilme;
    private RecyclerView recyclerView;

    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_filmes);

        firebaseAuth = FirebaseAuth.getInstance();

        lstFilme = new ArrayList<>();
        recyclerView = findViewById(R.id.recyclerviewwid);
        jasonrequest();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case R.id.logoutMenu:
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(TelaFilmes.this, MainActivity.class));
                break;
            case R.id.filmesLista:
                break;
            case R.id.favoritosLista:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void jasonrequest() {
        request = new JsonArrayRequest(JSON_URL, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                JSONObject jsonObject = null;

                for (int i = 0; i < response.length(); i++){

                    try {
                        jsonObject = response.getJSONObject(i);
                        Filme filme = new Filme();
                        filme.setTitulo_filme(jsonObject.getString("titulo"));
                        filme.setSubTitulo_filme(jsonObject.getString("subtitulo"));
                        filme.setDuracao_filme(jsonObject.getString("duracao"));
                        filme.setSinopse_filme(jsonObject.getString("sinopse"));
                        filme.setUrl_imagem(jsonObject.getString("img"));
                        lstFilme.add(filme);
                    } catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                setuprecyclerview(lstFilme);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });

        requestQueue = Volley.newRequestQueue(TelaFilmes.this);
        requestQueue.add(request);
        Toast.makeText(this,"MEU DEUS, FAZ PARAR", Toast.LENGTH_LONG);

    }

    private void setuprecyclerview(List<Filme> lstFilme) {

        RecyclerViewAdapter myadapter = new RecyclerViewAdapter(this, lstFilme);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(myadapter);
    }
}
