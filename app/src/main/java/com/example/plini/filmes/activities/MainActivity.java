package com.example.plini.filmes.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.plini.filmes.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    private TextView criarConta;
    private EditText login;
    private EditText senha;
    private Button confirma;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        criarConta = findViewById(R.id.textViewCadastro);
        login = findViewById(R.id.editTextLogin);
        senha = findViewById(R.id.editTextSenha);
        confirma = findViewById(R.id.buttonLogin);

        firebaseAuth = FirebaseAuth.getInstance();

        FirebaseUser user = firebaseAuth.getCurrentUser();

        //O app vai funcionar da forma que quando alguem loga, deve deslogar para que
        //outra pessoa possa entrar, nao somente fechar o app.
        if(user != null){
            finish();
            startActivity(new Intent(MainActivity.this, TelaFilmes.class));
        }

        confirma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().isEmpty() || senha.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "Preencha todos os campos", Toast.LENGTH_LONG).show();
                }else{
                    confirmacao(login.getText().toString(), senha.getText().toString());
                }
            }
        });

        criarConta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, CriarConta.class));
            }
        });
    }

    private void confirmacao(String usuario, String senha){

        firebaseAuth.signInWithEmailAndPassword(usuario, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Toast.makeText(MainActivity.this, "Logado com sucesso", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this, TelaFilmes.class));
                }else{
                    Toast.makeText(MainActivity.this, "Falha ao tentar logar", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
